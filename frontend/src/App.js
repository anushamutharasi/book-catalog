
import './App.css';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"
import Navbar from './Components/Navbar';
import Home from './Components/Home';
import Addbook from './Components/Addbook';
import {Switch,Route} from "react-router-dom"
import Edit from './Components/Edit';
import Singlebook from './Components/singlebook';
function App() {
  return (
    <div className="App">
    <Navbar/>
    <Switch>
  
    <Route exact path={"/"} component={Home}></Route>
    <Route  exact path={"/addbook"} component={Addbook}></Route>
    <Route exact path={"/edit/:id"} component={Edit}></Route>
    <Route exact path={"/book/:id"} component={Singlebook}></Route>

    </Switch>
    </div>
  );
}

export default App;