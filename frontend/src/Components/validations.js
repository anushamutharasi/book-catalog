



/*
 * Helper function to validate security_answer string
 * checks if security_question is string, else returns "security_answer should be of string"
 * @param {}security_answer
 * @returns
 */

const name_validator=(name)=>{
  const errors={}
     if(!name){
       errors.name="book name is required"
     }
     return errors

}

const author_validator=(author)=>{
  const errors={}
     if(!author){
       errors.author="author name is required"
     }
     return errors

}

const description_validator=(description)=>{
  const errors={}
     if(!description){
       errors.description="description name is required"
     }
     return errors

}
const price_validator=(price)=>{
  const errors={}
     if(!price){
       errors.price="book price is required "
     }
     return errors

}

const discount_validator=(discount)=>{
  const errors={}
     if(!discount){
       errors.discount="discount  is required "
     }
     return errors

}
const publisher_name_validator=(publisher_name)=>{
  const errors={}
     if(!publisher_name){
       errors.publisher_name="PublisherName is required "
     }
     return errors

}
const available_validator=(available)=>{
  const errors={}
     if(!available){
       errors.available="enter book is available or not "
     }
     return errors

}
const image_validator=(image)=>{
  const errors={}
     if(!image){
       errors.image="image url is required "
     }
     return errors

}

export {name_validator,author_validator,description_validator,price_validator,discount_validator,publisher_name_validator,available_validator,image_validator}