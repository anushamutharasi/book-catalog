import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import "../App.css";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";

const Singlebook = () => {
  const { id } = useParams("");
  console.log(id);
  const [getbookdata, setbookdata] = useState([]);
  console.log("getbookdata" + getbookdata);

  const getbookdatabyid = async (e) => {
    const res = await fetch(`http://localhost:4000/book/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 400 || !data) {
      alert("error");
    } else {
      setbookdata(data);
      console.log("get data");
    }
  };

  useEffect(() => {
    getbookdatabyid();
  }, []);

  return (
    <div className="container mt-3">
      {/* <Link to="/">Home</Link>    */}

      <Card sx={{ maxWidth:300, backgroundColor: " rgb(220, 112, 186)" }}>
        <CardContent>
        <div className="row">
          <img src={getbookdata.image} alt="book" className="image"></img>
         <div>
         <div className="left-view col-lg-6 col-md-6 col-12">
              <p>BookName:</p>
              <p>Author:</p>
              <p>Description:</p>
              <p>Price:</p>
              <p>Discount:</p>
              <p>PublisherName:</p>
              <p>Availability:</p>
            </div>
            <div className="right-view col-lg-6 col-md-6 col-12">
              <p>{getbookdata.name}</p>
              <p>{getbookdata.author}</p>
              <p>{getbookdata.description}</p>
              <p>{getbookdata.price}</p>
              <p>{getbookdata.discount}</p>
              <p>{getbookdata.publisher_name}</p>
              <p>{getbookdata.available}</p>
            </div>

         </div>
                      </div>
        </CardContent>
      </Card>
    </div>
  );
};

export default Singlebook;
