import React from "react";
import { Link, useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

const Edit = () => {
  //   const [getbookdata, setbookdata] = useState([]);

  //   console.log(getbookdata);

  const history=useHistory("")
  const { id } = useParams();
  console.log(id);
  const [inpval, setinpval] = useState({
    name: "",
    author: "",
    description: "",
    price: "",
    discount: "",
    publisher_name: "",
    available: "",
    image: "",
  });

  const setdata = (e) => {
    console.log(e.target.value);
    const { name, value } = e.target;
    setinpval((preval) => {
      return {
        ...preval,
        [name]: value,
      };
    });
  };

  const getbookdatabyid = async (e) => {
    const res = await fetch(`http://localhost:4000/book/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 400 || !data) {
      alert("error");
    } else {
      setinpval(data);
      console.log("get data");
    }
  };

  useEffect(() => {
    getbookdatabyid();
  }, []);

  const UpdateUser = async (e) => {
    e.preventDefault();
    const {
      name,
      author,
      description,
      price,
      discount,
      publisher_name,
      available,
      image,
    } = inpval;

    const new_response = await fetch(`http://localhost:4000/update/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        author,
        description,
        price,
        discount,
        publisher_name,
        available,
        image,
      }),
    });

    const update_data = new_response.json();
    console.log(update_data);
    if (new_response.status === 400 || !update_data) {
      alert("fill the data");
    } else {
      alert("book updated successfully");
      history.push("/")
    }
  };

  return (
    <div className="container">
      <Link to="/">Home</Link>
      <div>Edit page</div>
      <form className="mt-4">
        <div class="row">
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              BookName
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="name"
              value={inpval.name}
              onChange={setdata}
            />
          </div>

          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              AuthorName
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="author"
              value={inpval.author}
              onChange={setdata}
            />
          </div>
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Description
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="description"
              value={inpval.description}
              onChange={setdata}
            />
          </div>
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Price
            </label>
            <input
              type="number"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="price"
              value={inpval.price}
              onChange={setdata}
            />
          </div>
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Discount
            </label>
            <input
              type="number"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="discount"
              value={inpval.discount}
              onChange={setdata}
            />
          </div>
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              PublisherName
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="publisher_name"
              value={inpval.publisher_name}
              onChange={setdata}
            />
          </div>
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Availability
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="available"
              value={inpval.available}
              onChange={setdata}
            />
          </div>

          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              ImageUrl
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="image"
              value={inpval.image}
              onChange={setdata}
            />
          </div>
          <button
            type="submit"
            onClick={UpdateUser}
            className="btn btn-primary"
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default Edit;
