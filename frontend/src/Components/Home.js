import React from "react";
import { BiEdit } from "react-icons/bi";
import { MdDelete } from "react-icons/md";
import { AiFillEye } from "react-icons/ai";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

const Home = () => {
  const [getbooksdata, setbooksdata] = useState([]);

  console.log(getbooksdata);

  const getdata = async (e) => {
    const res = await fetch("http://localhost:4000/getbooks", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();

    if (res.status === 400 || !data) {
      alert("error");
    } else {
      setbooksdata(data);
      console.log("get data");
    }
  };
  useEffect(() => {
    getdata();
  }, []);
const deletebook= async(id)=>{

const res2=await fetch(`http://localhost:4000/delete/${id}`,{

  method: "DELETE",
  headers: {
    "Content-Type": "application/json",
  },



});
const delete_data=await res2.json();
console.log(delete_data);
if(res2===400 || !delete_data)
{
  alert("error")
}
else{
alert("book deleted")
getdata();
}
}
  return (
    <div className="mt-6">
      <div className="container">
        <div className="add_btn mt-2 mb-2">
          <Link to="/addbook">
            <button className="btn btn-primary">Add Book</button>
          </Link>
        </div>

        <table class="table">
          <thead>
            <tr className="table-dark">
              <th scope="col">id</th>
              <th scope="col">BookName</th>
              <th scope="col">AuthorName</th>
              <th scope="col">Description</th>
              <th scope="col">Price</th>
              <th scope="col">Discount</th>
              <th scope="col">PublisherName</th>
              <th scope="col">availability</th>

              <th></th>
            </tr>
          </thead>
          <tbody>
            {getbooksdata.map((book, index) => {
              return (
                <>
                  <tr>
                    <th scope="row">{index + 1}</th>

                    <td>{book.name}</td>
                    <td>{book.author}</td>
                    <td>{book.description}</td>
                    <td>{book.price}</td>
                    <td>{book.discount}</td>
                    <td>{book.publisher_name}</td>
                    <td>{book.available}</td>

                    <td className="d-flex justify-content-between">
                      <Link to={`/book/${book._id}`}>
                        <button className="btn btn-success mx-2">
                          <AiFillEye />
                          see book
                        </button>
                      </Link>
                      <Link to={`/edit/${book._id}`}>
                        <button className="btn btn-primary mx-2">
                          <BiEdit />
                          update
                        </button>
                      </Link>
                      <button   className="btn btn-danger mx-2" onClick={()=>deletebook(book._id)}>
                        <MdDelete />
                        delete
                      </button>
                    
                  
                    </td>
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Home;
