import React from "react";
import { Link, useHistory } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
import { name_validator ,author_validator,
description_validator,price_validator,discount_validator,
publisher_name_validator,available_validator,
image_validator} from "./validations";
import "../App.css"

const Addbook = () => {
  const history=useHistory("")
  const [inpval, setinpval] = useState({
    name: "",
    author: "",
    description: "",
    price: "",
    discount: "",
    publisher_name: "",
    available: "",
    image: "",
  });
  const [formerrors,setformerrors]=useState({

  })
 const setdata = (e) => {
    console.log(e.target.value);
    const { name, value } = e.target;
    setinpval((preval) => {
      return {
        ...preval,
        [name]: value,
      };
    });
  };

  useEffect(() => {
    console.log(formerrors);

    if(Object.keys(formerrors).length===0){
    console.log(inpval);
    }
  },[formerrors]);


const {name,author,description,price,discount,publisher_name,available,image}=inpval
let err_obj={
  ...name_validator(name),
  ...author_validator(author),
  ...description_validator(description),
  ...price_validator(price),
  ...discount_validator(discount),
  ...publisher_name_validator(publisher_name),
  ...available_validator(available),
  ...image_validator(image)

}
  



  const addinpdata = async (e) => {
    e.preventDefault();
 //setformerrors(err_obj)
    const {
      name,
      author,
      description,
      price,
      discount,
      publisher_name,
      available,
      image,
    } = inpval;
    const res = await fetch("http://localhost:4000/addbook", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        author,
        description,
        price,
        discount,
        publisher_name,
        available,
        image,
      }),
    });
    const data = await res.json();
    console.log(data);
    if (res.status === 400) {
    setformerrors(err_obj)
    } else {
      alert("book added sucessfully")
      history.push("/")
    }
  };

  return (
    <div className="container">
   
      <form className="mt-4">
        <div class="row">
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              BookName
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="name"
              value={inpval.name}
              onChange={setdata}
              
            />
            <span style={{color:"red"}}>{formerrors.name}</span>
          </div>
     

          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              AuthorName
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="author"
              value={inpval.author}
              onChange={setdata}
            />
            <span style={{color:"red"}}>{formerrors.author}</span> 
          </div>
          
                  <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Description
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="description"
              value={inpval.description}
              onChange={setdata}
            />
              <span style={{color:"red"}}>{formerrors.description}</span>
          </div>
        
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Price
            </label>
            <input
              type="number"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="price"
              value={inpval.price}
              onChange={setdata}
            />
              <span style={{color:"red"}}>{formerrors.price}</span>
          </div>
        
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Discount
            </label>
            <input
              type="number"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="discount"
              value={inpval.discount}
              onChange={setdata}
            />
              <span style={{color:"red"}}>{formerrors.discount}</span>
          </div>
        
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              PublisherName
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="publisher_name"
              value={inpval.publisher_name}
              onChange={setdata}
            />
           <span style={{color:"red"}}>{formerrors.publisher_name}</span>
          </div>
    
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              Availability
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="available"
              value={inpval.available}
              onChange={setdata}
            />
            <span style={{color:"red"}}>{formerrors.available}</span>
          </div>
         
          <div class="mb-3 col-lg-6 col-lg-6  col-12">
            <label for="exampleInputEmail1" class="form-label">
              ImageUrl
            </label>
            <input
              type="text"
              class="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              name="image"
              value={inpval.image}
              onChange={setdata}
            />
            <span style={{color:"red"}}>{formerrors.image}</span>
          </div>
          
          <button
            type="submit"
            onClick={addinpdata}
            className="btn btn-primary"
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default Addbook;
