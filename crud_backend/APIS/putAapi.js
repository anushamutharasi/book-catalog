import book_model from "../Database/models.js";


const update_book_details=(req,res)=>{
  book_model.findByIdAndUpdate(
    req.params.id,
    {
      $set: req.body,
    },
    (err,updateobj)=>{
      if(err)
      {
        res.status(400).send(err)
      }
      else{
        res.status(200).send(updateobj)
      }
    }
  )




}

export{update_book_details}