import book_model from "../Database/models.js";


const delete_book_details=(req,res)=>{
  book_model.findByIdAndRemove(req.params.id,(err,deletedobj)=>{
    if(err){
      res.status(400).send(err)
    }
    else{
      res.status(200).send(deletedobj)
    }

  })

}
export {delete_book_details}