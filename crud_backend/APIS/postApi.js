import mongoose from "mongoose";
import book_model from "../Database/models.js";

const posting_book_details=(req,res)=>{
  
 const book_details=req.body

  const new_book_details=new book_model(book_details)
  new_book_details.save()
  .then((data)=>{
    res.status(200).send(data)
  })
  .catch(err=>{
    res.status(400).send({err:err})
  })
}

export {posting_book_details};