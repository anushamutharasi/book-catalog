import book_model from "../Database/models.js";


const getting_book_details=(req,res)=>{
  book_model.find({},(err,data)=>{
    if(err){
    res.status(400).send(err)
    }
    else{
      //this posts variable is used to get all books and it is used in ui part also
      res.status(200).send(data)
    }
  })
}

  const getting_book_details_byid=(req,res)=>{
    const {id}=req.params
    book_model.findById({_id:id},(err,bookdata)=>{
      if(err){
        res.status(400).send(err)
      }
      else{
        res.status(200).send(bookdata)
      }
    })

}
export {getting_book_details,getting_book_details_byid}