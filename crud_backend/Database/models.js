import  mongoose from "mongoose";
import bookSchema from "./schemas.js";

//creating a model for book schema
const book_model=mongoose.model("Book",bookSchema)


export default book_model;