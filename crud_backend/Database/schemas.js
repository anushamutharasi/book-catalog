import mongoose from "mongoose";

const Schema = mongoose.Schema;

const bookSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  discount: {
    type: Number,
    required: true,
  },
  publisher_name: {
    type: String,
    required: true,
  },
  available: {
    type: String,
    required:true,
  },
  image: {
    type: String,
    required: true,
  },
});
export default bookSchema;

// books
