import express from "express";
import mongoose from "mongoose";
import cors from "cors"
import { delete_book_details } from "./APIS/deleteApi.js";
import { getting_book_details, getting_book_details_byid } from "./APIS/getApi.js";
import { posting_book_details } from "./APIS/postApi.js";
import { update_book_details } from "./APIS/putAapi.js";
const app=express()
app.use(express.json())
app.use(cors())
const port=4000

mongoose.connect("mongodb+srv://una:una@cluster0.6p3ky.mongodb.net/CRUD?retryWrites=true&w=majority",()=>{
  console.log("connected to db");

})
app.get("/book/:id",getting_book_details_byid)
app.get("/getbooks",getting_book_details)


app.post("/addbook",posting_book_details)
app.put("/update/:id",update_book_details)
app.delete("/delete/:id",delete_book_details)

app.listen(port,()=>{
  console.log("server running at"+`${port}`);
})

